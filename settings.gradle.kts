rootProject.name = "scripts"

include("lom")
include("convention")
include("jfmod")

enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")
