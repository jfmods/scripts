plugins {
    `java-gradle-plugin`
    id("jf.maven-publish")
    id("org.gradle.kotlin.kotlin-dsl")
}

group = rootProject.group
version = rootProject.version

repositories {
    mavenCentral()
    gradlePluginPortal()
    maven {
        name = "JFronny Mirrors"
        url = uri("https://maven.frohnmeyer-wds.de/mirrors")
    }
}
