// Placeholder to support jf.maven-publish linked from convention/

plugins {
    base
}

tasks.withType<AbstractArchiveTask>().configureEach {
    isPreserveFileTimestamps = false
    isReproducibleFileOrder = true
}