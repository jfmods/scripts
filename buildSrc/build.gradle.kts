@file:Suppress("INVISIBLE_MEMBER", "INVISIBLE_REFERENCE")

import org.gradle.kotlin.dsl.plugins.appliedKotlinDslPluginsVersion

plugins {
    `kotlin-dsl`
}

repositories {
    mavenCentral()
    gradlePluginPortal()
}

dependencies {
    implementation("org.gradle.kotlin:gradle-kotlin-dsl-plugins:$appliedKotlinDslPluginsVersion")
}