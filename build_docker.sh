#!/bin/bash
dir="$(dirname "$0")"
docker buildx build --push -f "$dir/docker/ci-wine.Dockerfile" -t git.frohnmeyer-wds.de/johannes/ci-wine "$dir"