plugins {
    jf.`plugin-conventions`
}

dependencies {
    implementation("org.eclipse.jgit:org.eclipse.jgit:7.2.0.202503040940-r") // https://github.com/eclipse-jgit/jgit/tags
    implementation("com.palantir.javapoet:javapoet:0.6.0") // https://github.com/palantir/javapoet/releases
}