plugins {
    jf.`plugin-conventions`
}

dependencies {
    implementation("org.eclipse.jgit:org.eclipse.jgit:7.1.0.202411261347-r")
    implementation("com.palantir.javapoet:javapoet:0.5.0")
}