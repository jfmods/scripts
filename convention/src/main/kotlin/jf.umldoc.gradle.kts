import io.gitlab.jfronny.scripts.*;

plugins {
    `java-library`
}

val umldoc by configurations.creating

dependencies {
    umldoc("nl.talsmasoftware:umldoclet:2.2.1")
    umldoc("org.jdrupes.taglets:plantuml-taglet:3.1.0")
}

tasks.javadoc {
    source = sourceSets.main.get().allJava
    opts.docletpath = umldoc.files.toList()
    opts.doclet = "nl.talsmasoftware.umldoclet.UMLDoclet"
    opts.tagletPath = umldoc.files.toList()
    opts.taglets("org.jdrupes.taglets.plantUml.PlantUml", "org.jdrupes.taglets.plantUml.StartUml", "org.jdrupes.taglets.plantUml.EndUml")
}

java {
    withJavadocJar()
}