package io.gitlab.jfronny.scripts

import org.eclipse.jgit.api.Git
import org.eclipse.jgit.errors.IncorrectObjectTypeException
import org.eclipse.jgit.lib.*
import org.eclipse.jgit.revwalk.RevCommit
import org.eclipse.jgit.revwalk.RevTag
import org.eclipse.jgit.revwalk.RevWalk
import org.eclipse.jgit.storage.file.FileBasedConfig
import org.eclipse.jgit.util.FS
import org.eclipse.jgit.util.SystemReader
import java.time.Instant
import java.time.ZoneOffset
import java.time.ZonedDateTime

fun initializeGit() {
    val original = SystemReader.getInstance()
    SystemReader.setInstance(object : SystemReader() {
        override fun getHostname(): String = original.hostname
        override fun getenv(key: String): String? = if (key == "PATH") null else original.getenv(key)
        override fun getProperty(key: String?): String? = original.getProperty(key)
        override fun openUserConfig(parent: Config?, fs: FS?): FileBasedConfig = original.openUserConfig(parent, fs)
        override fun openSystemConfig(parent: Config?, fs: FS?): FileBasedConfig = original.openSystemConfig(parent, fs)
        override fun openJGitConfig(parent: Config?, fs: FS?): FileBasedConfig = original.openJGitConfig(parent, fs)
        override fun getCurrentTime(): Long = original.currentTime
        override fun getTimezone(`when`: Long): Int = original.getTimezone(`when`)
    })
    FS.DETECTED.gitSystemConfig
}

fun Git.log(since: AnyObjectId? = null, until: AnyObjectId? = null): List<Commit> {
    var command = this.log()
    if (since != null) command = command.not(since)
    if (until != null) command = command.add(until)
    return command.call().map { it.resolve(repository) }
}

fun Git.getTags(): List<Tag> = repository.refDatabase.getRefsByPrefix(Constants.R_TAGS)
    .map { it.resolveTag(repository) }
    .filter { it.checkInTree(repository) }
    .sortedByDescending { it.dateTime }

fun Ref.resolveTag(repo: Repository): Tag = RevWalk(repo).use { walk ->
    val rev = try {
        walk.parseTag(objectId)
    } catch (e: IncorrectObjectTypeException) {
        // Lightweight (unannotated) tag
        // Copy information from commit
        val target = walk.parseCommit(objectId).resolve(repo)
        return Tag(objectId, null, name, target, target.author, null, null, target.dateTime)
    }
    walk.parseBody(rev)
    walk.parseBody(rev.`object`)
    val target = walk.peel(rev)
    walk.parseBody(target)
    return rev.resolve((target as RevCommit).resolve(repo))
}

fun Tag.checkInTree(repo: Repository): Boolean = RevWalk(repo).use { walk ->
    walk.isMergedInto(walk.parseCommit(commit.id), walk.parseCommit(repo.resolve("HEAD")))
}

fun RevTag.resolve(commit: Commit): Tag {
    val ident = taggerIdent
    return Tag(
        id,
        this,
        tagName,
        commit,
        Person(ident, ident.name, ident.emailAddress),
        fullMessage.nullable,
        shortMessage.nullable,
        ZonedDateTime.ofInstant(
            ident.`when`.toInstant(),
            ident.timeZone?.toZoneId() ?: ZoneOffset.UTC
        )
    )
}

fun RevCommit.resolve(repo: Repository): Commit = Commit(
    this,
    id,
    try {
        repo.newObjectReader().use { reader -> reader.abbreviate(this).name() }
    } catch (e: Exception) {
        Exception("Could not abbreviate $name", e).printStackTrace()
        null
    },
    Person(committerIdent, committerIdent.name, committerIdent.emailAddress),
    Person(authorIdent, authorIdent.name, authorIdent.emailAddress),
    ZonedDateTime.ofInstant(
        Instant.ofEpochSecond(commitTime.toLong()),
        committerIdent.timeZone?.toZoneId() ?: ZoneOffset.UTC
    ),
    fullMessage,
    shortMessage,
    parents?.map { ObjectId.toString(it) } ?: emptyList()
)

private val String?.nullable get() = if (this == null || this == "") null else this

data class Tag(val id: ObjectId, val original: RevTag?, val fullName: String, val commit: Commit, val tagger: Person, val fullMessage: String?, val shortMessage: String?, val dateTime: ZonedDateTime?) {
    val name: String get() = Repository.shortenRefName(fullName)
    val peeledId: ObjectId get() = commit.original.id
}
data class Commit(val original: RevCommit, val id: ObjectId, val abbreviatedId: String?, val committer: Person, val author: Person, val dateTime: ZonedDateTime, val fullMessage: String, val shortMessage: String, val parentIds: List<String>)
data class Person(val original: PersonIdent, val name: String, val emailAddress: String)

val Commit.isMerge get() = parentIds.size > 1