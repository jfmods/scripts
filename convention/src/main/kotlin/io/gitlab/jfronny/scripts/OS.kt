package io.gitlab.jfronny.scripts

object OS {
    val TYPE: Type = with(System.getProperty("os.name", "generic").toLowerCase()) {
        when {
            contains("mac") || contains("darwin") -> Type.MAC_OS
            contains("win") -> Type.WINDOWS
            contains("nux") -> Type.LINUX
            else -> throw UnsupportedOSException()
        }
    }

    enum class Type(val displayName: String, val codename: String) {
        WINDOWS("Windows", "windows"),
        MAC_OS("OSX", "macos"),
        LINUX("Linux", "linux");
    }

    class UnsupportedOSException : RuntimeException("Unrecognized OS")
}