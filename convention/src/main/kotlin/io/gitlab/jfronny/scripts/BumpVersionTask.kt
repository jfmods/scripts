package io.gitlab.jfronny.scripts

import org.eclipse.jgit.api.Git
import org.gradle.api.DefaultTask
import org.gradle.api.file.DirectoryProperty
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputDirectory
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.TaskAction

abstract class BumpVersionTask : DefaultTask() {
    init {
        description = "Bumps the version by parsing commits since the last tag and creating a new tag based on them"
    }

    @get:InputDirectory abstract val gitDir: DirectoryProperty
    @get:Input abstract val releaseWasSet: Property<Boolean> //TODO rename back to isRelease
    @get:Input abstract val nextVersionType: Property<String>
    @get:Input abstract val nextRelease: Property<SemanticVersion>
    @get:Input abstract val lastRelease: Property<String>
    @get:Input @get:Optional abstract val typeSupplement: Property<String>

    @TaskAction
    fun action() {
        if (!gitDir.asFile.get().exists()) throw IllegalStateException("Cannot bump without repository")
        if (releaseWasSet.get()) throw IllegalStateException("Cannot bump while 'release' is set")
        val vt = nextVersionType
            .map { VersionType.byName(it) ?: throw IllegalStateException("Unrecognized version type: $this") }
            .getOrThrow { IllegalStateException("bumpVersion requires you to set -PversionType=release|beta|alpha") }
        val name = nextRelease.get().withType(vt, typeSupplement.orNull).toString()
        Git.open(gitDir.asFile.get()).use { git ->
            git.tag().setName(name).call()
            logger.warn("Created release $name (last was ${lastRelease.get()}). Make sure to push it!")
        }
    }
}