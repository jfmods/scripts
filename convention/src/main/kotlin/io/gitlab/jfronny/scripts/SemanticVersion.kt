package io.gitlab.jfronny.scripts

/**
 * A semantic version representation.
 * Not strictly following the semver spec, but a subset of it.
 * For example, the pre-release type identifier is restricted to alpha, beta, rc followed by an optional number.
 * Parsing also supports legacy version strings like v1.2.3, b1.2.3, a1.2.3, rc1.2.3.
 */
data class SemanticVersion(val major: Int, val minor: Int, val patch: Int, val type: VersionType, val typeSupplement: String?, val build: String?): Comparable<SemanticVersion> {
    constructor(major: Int, minor: Int, patch: Int, versionType: VersionType) : this(major, minor, patch, versionType, null, null)
    constructor(major: Int, minor: Int, patch: Int) : this(major, minor, patch, VersionType.RELEASE)

    init {
        require(build == null || buildPattern.matches(build)) { "Illegal build string" }
    }

    override fun compareTo(other: SemanticVersion): Int = compareValuesBy(this, other, SemanticVersion::major, SemanticVersion::minor, SemanticVersion::patch, SemanticVersion::type, SemanticVersion::typeSupplement, SemanticVersion::build)

    override fun toString(): String {
        return "$major.$minor.$patch" + when (type) {
            VersionType.RELEASE -> typeSupplement.possiblyPrefix("-")
            else -> "-${type.semanticName}" + typeSupplement.possiblyPrefix(".")
        } + build.possiblyPrefix("+")
    }

    private fun String?.possiblyPrefix(prefix: String) = if (this == null) "" else "$prefix$this"

    fun unclassifiedToString(): String {
        return "$major.$minor.$patch" + if (build == null) "" else "+$build"
    }

    fun incrementBy(commitType: CommitType, versionType: VersionType = VersionType.RELEASE, build: String? = null): SemanticVersion = when(commitType) {
        CommitType.FIX -> SemanticVersion(major, minor, patch + 1, versionType, null, build)
        CommitType.FEAT -> SemanticVersion(major, minor + 1, 0, versionType, null, build)
        CommitType.BREAKING -> SemanticVersion(major + 1, 0, 0, versionType, null, build)
    }

    fun withType(versionType: VersionType, typeSupplement: String? = null) = SemanticVersion(major, minor, patch, versionType, typeSupplement, build)
    fun withBuild(build: String?) = SemanticVersion(major, minor, patch, type, typeSupplement, build)

    companion object {
        private val identifier = Regex("[a-zA-Z1-9][a-zA-Z0-9]*")
        private val buildPattern = Regex("$identifier(?:\\.$identifier)*")
        private val number = Regex("[1-9][0-9]*|0")
        private val versionCore = Regex("($number)\\.($number)(?:\\.($number))?")
        private val legacyVersion = Regex("([vba]|rc)$versionCore(\\+$buildPattern)?")
        private val restrictedSemver = Regex("$versionCore(?:-(alpha|beta|rc|pre)(\\.?[a-zA-Z0-9-]+)?)?(\\+$buildPattern)?")

        fun parse(source: String): SemanticVersion {
            return tryParse(source) ?: throw IllegalArgumentException("Source does not match supported version patterns: $source")
        }

        fun tryParse(source: String): SemanticVersion? {
            val legacyMatch = legacyVersion.matchEntire(source)
            if (legacyMatch != null) {
                val m = legacyMatch.groupValues
                return SemanticVersion(
                    m[2].toInt(), m[3].toInt(), m[4].ifEmpty { "0" }.toInt(),
                    VersionType.byShorthand(m[1])!!,
                    null,
                    m[5].ifEmpty { null }
                )
            }
            val semverMatch = restrictedSemver.matchEntire(source)
            if (semverMatch != null) {
                val m = semverMatch.groupValues
                return SemanticVersion(
                    m[1].toInt(), m[2].toInt(), m[3].ifEmpty { "0" }.toInt(),
                    VersionType.byName(m[4].ifEmpty { "release" })!!,
                    m[5].ifEmpty { null }?.trimStart('.'),
                    m[6].ifEmpty { null }?.substring(1)
                )
            }
            return null
        }
    }
}
