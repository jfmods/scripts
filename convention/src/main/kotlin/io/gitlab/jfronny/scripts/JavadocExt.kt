package io.gitlab.jfronny.scripts

import org.gradle.api.Project
import org.gradle.api.internal.catalog.DelegatingProjectDependency
import org.gradle.api.tasks.javadoc.Javadoc
import org.gradle.external.javadoc.StandardJavadocDocletOptions
import org.gradle.kotlin.dsl.named

val Javadoc.opts: StandardJavadocDocletOptions
    get() = options as? StandardJavadocDocletOptions
    ?: throw TypeCastException("Unexpected javadoc options type")

fun Javadoc.linksOffline(url: String, project: Project) {
    val task = project.tasks.named<Javadoc>("javadoc").get()
    opts.linksOffline(url, task.destinationDir.toString())
    dependsOn(task)
}

fun Javadoc.linksOffline(url: String, project: DelegatingProjectDependency) {
    val task = project.dependencyProject.tasks.named<Javadoc>("javadoc").get()
    opts.linksOffline(url, task.destinationDir.toString())
    dependsOn(task)
}