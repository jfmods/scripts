package io.gitlab.jfronny.scripts

import org.gradle.api.Action
import org.gradle.api.DefaultTask
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.TaskContainer
import org.gradle.kotlin.dsl.extra
import org.gradle.kotlin.dsl.named

var Project.versionS: String
    get() = version.toString()
    set(value) {
        version = value
    }

val Project.versionStripped: String get() = if (versionS.endsWith("-SNAPSHOT")) versionS.substring(0, versionS.length - "-SNAPSHOT".length) else versionS

var Project.versionType: VersionType
    get() = if (extra.has("versionType")) extra["versionType"] as VersionType else VersionType.RELEASE
    set(value) = extra.set("versionType", value)

var Project.lastRelease: String
    get() = if (extra.has("lastRelease")) extra["lastRelease"].toString() else ""
    set(value) = extra.set("lastRelease", value)

var Project.nextRelease: SemanticVersion?
    get() = if (extra.has("nextRelease")) extra["nextRelease"] as SemanticVersion else null
    set(value) = extra.set("nextRelease", value)

var Project.changelog: String
    get() = if (extra.has("changelog")) extra["changelog"].toString() else ""
    set(value) = extra.set("changelog", value)

var Project.changelogHtml: String
    get() = if (extra.has("changelogHtml")) extra["changelogHtml"].toString() else ""
    set(value) = extra.set("changelogHtml", value)

var Project.flavour: String
    get() = prop("flavour", "").trim()
    set(value) {
        if (hasProperty("flavour")) setProperty("flavour", value)
        else extra.set("flavour", value)
    }

val TaskContainer.deployDebug: Task get() = findByName("deployDebug") ?: register("deployDebug").get().dependsOn(named<DefaultTask>("build").get())
val TaskContainer.deployRelease: Task get() = findByName("deployRelease") ?: register("deployRelease").get().dependsOn(deployDebug)

fun Project.prop(name: String): String = property(name).toString()

fun Project.prop(name: String, default: String): String =
    if (hasProperty(name)) prop(name)
    else default

fun Project.nprop(name: String): String? =
    if (hasProperty(name)) prop(name)
    else null

// Utility to run actions on values
fun <T> T.runAction(action: Action<T>?): T {
    action?.execute(this!!)
    return this
}

fun <T> Provider<T>.getOrThrow(exception: () -> Exception): T =
    if (!isPresent) throw exception()
    else get()
