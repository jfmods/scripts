package io.gitlab.jfronny.scripts

import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.kotlin.dsl.extra

fun Project.earlyAfterEvaluate(action: Action<Project>) = earlyAfterEvaluates!!.add(action)
fun Project.insertEarlyAfterEvaluate(action: Action<Project>) = earlyAfterEvaluates!!.add(0, action)

var Project.earlyAfterEvaluates
    get() = extra["earlyAfterEvaluates"] as MutableList<Action<Project>>?
    set(value) { extra["earlyAfterEvaluates"] = value }