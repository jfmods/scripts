package io.gitlab.jfronny.scripts

import org.gradle.api.DefaultTask
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction
import java.awt.Toolkit
import java.awt.datatransfer.StringSelection

abstract class CopyTextTask : DefaultTask() {
    init {
        description = "Copies a piece of text to the system clipboard"
    }

    @get:Input abstract val text: Property<String>

    @TaskAction
    fun action() {
        Toolkit.getDefaultToolkit().systemClipboard.setContents(StringSelection(project.versionS), null)
        println("Copied version number: ${project.versionS}")
    }
}