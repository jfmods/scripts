package io.gitlab.jfronny.scripts

enum class VersionType(val displayName: String, val curseforgeName: String, val modrinthName: String, val semanticName: String, val shorthand: String): Comparable<VersionType> {
    ALPHA("Alpha", "alpha", "alpha", "alpha", "a"),
    BETA("Beta", "beta", "beta", "beta", "b"),
    PRERELEASE("Prerelease", "beta", "beta", "pre", "pre"),
    RELEASE_CANDIDATE("Release Candidate", "beta", "beta", "rc", "rc"),
    RELEASE("Release", "release", "release", "release", "v");

    companion object {
        private val byShorthand = values().associateBy { it.shorthand }
        private val byName = values().associateBy { it.semanticName }
        fun byShorthand(shorthand: String): VersionType? = byShorthand[shorthand]
        fun byName(name: String): VersionType? = byName[name]
    }
}