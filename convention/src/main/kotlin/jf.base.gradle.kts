import io.gitlab.jfronny.scripts.*

plugins {
    base
}

tasks.withType<AbstractArchiveTask>().configureEach {
    isPreserveFileTimestamps = false
    isReproducibleFileOrder = true
}

earlyAfterEvaluates = ArrayList()

afterEvaluate {
    earlyAfterEvaluates!!.forEach {
        it.execute(this)
    }
    earlyAfterEvaluates = null
}

tasks.register("printTaskGraph") {
    doLast {
        fun getTask(task: Any): Either<Task, String> {
            return when (task) {
                is Provider<*> -> getTask(task.get())
                is String -> Either.Left(tasks.getByPath(task))
                is Task -> Either.Left(task)
                else -> Either.Right("$task")
            }
        }

        fun printTaskGraph(task: Task, indent: Int, known: MutableSet<Task>) {
            if (known.contains(task)) {
                println("  ".repeat(indent) + "- ${task.name} (*)")
                return
            }
            known.add(task)
            println("  ".repeat(indent) + "- ${task.name}")
            task.dependsOn.forEach {
                getTask(it).apply({ dependency ->
                    printTaskGraph(dependency, indent + 1, known)
                }, { error ->
                    println("  ".repeat(indent + 1) + "- UNEXPECTED!: $error")
                })
            }
        }

        printTaskGraph(tasks.getByPath(prop("taskGraph", "build")), 0, LinkedHashSet())
    }
}