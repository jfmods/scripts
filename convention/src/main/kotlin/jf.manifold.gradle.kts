import io.gitlab.jfronny.scripts.earlyAfterEvaluate

plugins {
    id("jf.java")
}

val manifoldVersion by extra("2025.1.5") // https://github.com/manifold-systems/manifold/tags

dependencies {
    implementation("systems.manifold:manifold-props-rt:$manifoldVersion")

    testAnnotationProcessor(annotationProcessor("systems.manifold:manifold-ext:$manifoldVersion")!!)
    testAnnotationProcessor(annotationProcessor("systems.manifold:manifold-props:$manifoldVersion")!!)
}

earlyAfterEvaluate {
    if (sourceSets.main.get().allJava.files.any {it.name == "module-info.java"}) {
        tasks.withType<JavaCompile> {
            options.compilerArgs.addAll(arrayOf("-Xplugin:Manifold no-bootstrap", "--module-path", classpath.asPath))
        }
    } else {
        tasks.withType<JavaCompile> {
            options.compilerArgs.addAll(arrayOf("-Xplugin:Manifold no-bootstrap"))
        }
    }
}
