import io.gitlab.jfronny.scripts.*
import org.eclipse.jgit.api.Git
import kotlin.jvm.optionals.getOrDefault

val isRelease = project.hasProperty("release")

val specialCaseBuilds = setOf("forge", "fabric", "neoforge")
fun List<Tag>.filter(): Pair<Tag, Tag?>? {
    if (isEmpty()) return null
    val envName = System.getenv("CI_COMMIT_TAG")?.ifEmpty { null }
    val current = envName?.let { v -> indexOfFirst { it.name == v } } ?: 0
    if (current == -1) throw IllegalStateException("Current tag ($envName) not found in tag list")
    if (size == current + 1) return get(current) to null

    val ver = SemanticVersion.tryParse(get(current).name)
    // special case: some mod projects use the format 1.0.0+forge to classify builds for different loaders
    // this ensures changelogs get generated within the same loader branch
    if (ver?.build in specialCaseBuilds) {
        val preceding = drop(current + 1).firstOrNull { SemanticVersion.tryParse(it.name)?.build == ver?.build }
        if (preceding != null) return get(current) to preceding
    }

    return get(current) to get(current + 1)
}

versionS = "0.0.0+nogit"
versionType = VersionType.ALPHA
if (File(projectDir, ".git").exists()) {
    initializeGit()
    Git.open(projectDir).use { git ->
        versionS = "0.0.0+notag"
        val tags = git.getTags().filter()
        if (tags != null) {
            if (tags.first.fullMessage != null) {
                changelog += "${tags.first.fullMessage}\n"
                changelogHtml += "<p>${tags.first.fullMessage}</p>\n"
            }
            versionS = tags.first.name
            lastRelease = versionS
            val parsedVersion = SemanticVersion.parse(versionS)
            versionType = parsedVersion.type
            versionS = parsedVersion.unclassifiedToString()
            if (isRelease) {
                changelog += "Commits in ${versionType.displayName} $versionS:\n"
                val log = git.log(tags.second?.peeledId, tags.first.peeledId).reversed()
                changelog += log.joinToString("\n") { "- ${it.shortMessage}" }
                changelogHtml += "<ul>\n" + log.joinToString("") { "  <li>${it.shortMessage}</li>\n" } + "</ul>"
                nextRelease = parsedVersion
            } else {
                changelog += "Commits after ${versionType.displayName} $versionS:\n"
                val log = git.log(tags.first.peeledId, git.repository.resolve("HEAD")).reversed()
                changelog += log.joinToString("\n") { "- ${it.shortMessage}" }
                changelogHtml += "<ul>\n" + log.joinToString("") { "  <li>${it.shortMessage}</li>\n" } + "</ul>"
                val type = log.stream()
                    .filter { !it.isMerge }
                    .map { it.fullMessage }
                    .map { msg -> CommitType.from(msg) { logger.warn(it) } }
                    .max { o1, o2 -> o1.compareTo(o2) }
                    .getOrDefault(CommitType.FIX)
                nextRelease = parsedVersion.incrementBy(type, build = nprop("buildClassifier"))
            }
        } else {
            changelog += "Commits after inception:\n"
            val log = git.log().all().call().reversed()
            changelog += log.joinToString("\n") { "- ${it.shortMessage}" }
            changelogHtml += "<ul>\n" + log.joinToString("") { "  <li>${it.shortMessage}</li>\n" } + "</ul>"
        }
    }
} else {
    changelog = "No changelog"
    changelogHtml = "<p>No changelog</p>"
}

if (!isRelease) {
    versionS = "$nextRelease-SNAPSHOT"
}

println(changelog)

tasks.register("copyVersionNumber", CopyTextTask::class) {
    description = "Copy the current version number to the system clipboard"
    text.set(versionS)
}

tasks.register("bumpVersion", BumpVersionTask::class) {
    gitDir = File(projectDir, ".git")
    releaseWasSet = this@Jf_autoversion_gradle.isRelease
    nextVersionType = project.provider { nprop("nextVersionType") }
    nextRelease = project.provider { project.nextRelease }
    lastRelease = project.provider { project.lastRelease }
    typeSupplement = project.provider { nprop("typeSupplement") }
}
