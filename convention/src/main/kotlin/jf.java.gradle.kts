import io.gitlab.jfronny.scripts.ContentGenerator
import io.gitlab.jfronny.scripts.codegenDir

plugins {
    id("jf.base")
    `java-library`
}

var codeGenerators: MutableMap<String, ContentGenerator.Generated>? by extra(LinkedHashMap())

val jfCodegen by tasks.registering {
    doLast {
        if (codegenDir.exists()) codegenDir.deleteRecursively()
        codeGenerators!!.forEach { (name, generated) ->
            generated.classes.forEach { (filePath, content) ->
                val path = codegenDir.resolve("java").resolve(name).resolve(filePath)
                path.parentFile.mkdirs()
                path.writeText(content)
            }
            generated.resources.forEach { (filePath, content) ->
                val path = codegenDir.resolve("resources").resolve(name).resolve(filePath)
                path.parentFile.mkdirs()
                path.writeBytes(content)
            }
        }
        codeGenerators = null
    }
}

tasks.compileJava { dependsOn(jfCodegen) }

tasks.processResources { dependsOn(jfCodegen) }