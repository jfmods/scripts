plugins {
    id("jf.base")
    `maven-publish`
}

publishing {
    repositories {
        mavenLocal()

        if (project.hasProperty("maven")) {
            maven {
                url = uri(project.property("maven").toString())
                name = "dynamic"

                credentials(PasswordCredentials::class) {
                    username = System.getenv()["MAVEN_NAME"]
                    password = System.getenv()["MAVEN_TOKEN"]
                }
                authentication {
                    create<BasicAuthentication>("basic")
                }
            }
        }
    }
}