package io.gitlab.jfronny.scripts

import java.util.*

enum class LoaderKind(val curseforgeName: String, val modrinthName: String) {
    NEOFORGE("NeoForge", "neoforge"),
    FORGE("Forge", "forge"),
    FABRIC("Fabric", "fabric");

    companion object {
        fun fromString(str: String) = valueOf(str.uppercase(Locale.getDefault()))
    }
}
