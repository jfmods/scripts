package io.gitlab.jfronny.scripts

import org.gradle.api.Project
import org.gradle.api.provider.Property
import java.util.*

interface LomExtension {
    val loaderKind: Property<LoaderKind>
    val minecraftVersion: Property<String>
    val loaderVersion: Property<String>
    val yarnBuild: Property<String>
    val neoforgeYarnPatch: Property<String>

    fun yarn(yarnBuild: String) {
        this.yarnBuild.set(Objects.requireNonNull(yarnBuild))
    }

    fun mojmap() {
        yarnBuild.set(null)
    }

    fun check(proj: Project) {
        yarnBuild.finalizeValue()
        minecraftVersion.finalizeValue()
        loaderVersion.finalizeValue()
        loaderKind.finalizeValue()
        neoforgeYarnPatch.finalizeValue()
    }

    fun copyFrom(ext: LomExtension) {
        yarnBuild.convention(ext.yarnBuild)
        minecraftVersion.convention(ext.minecraftVersion)
        if (loaderKind.get() == ext.loaderKind.get()) {
            loaderVersion.convention(ext.loaderVersion)
        }
        neoforgeYarnPatch.convention(ext.neoforgeYarnPatch)
    }
}