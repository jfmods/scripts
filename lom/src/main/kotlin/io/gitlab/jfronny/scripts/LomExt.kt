package io.gitlab.jfronny.scripts

import org.gradle.api.DefaultTask
import org.gradle.api.artifacts.ProjectDependency
import org.gradle.api.artifacts.dsl.DependencyHandler
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.api.tasks.TaskContainer
import org.gradle.api.tasks.bundling.Jar
import org.gradle.kotlin.dsl.named
import org.gradle.kotlin.dsl.project

fun DependencyHandler.testmodImplementation(dependencyNotation: Any) = add("testmodImplementation", dependencyNotation)
fun DependencyHandler.clientImplementation(dependencyNotation: Any) = add("clientImplementation", dependencyNotation)

val SourceSetContainer.client get() = named<SourceSet>("client")
val SourceSetContainer.testmod get() = named<SourceSet>("testmod")

val TaskContainer.genClientOnlySources get() = named<DefaultTask>("genClientOnlySources")
val TaskContainer.genCommonSources get() = named<DefaultTask>("genCommonSources")
val TaskContainer.sourcesJar get() = named<Jar>("sourcesJar")

fun DependencyHandler.devProject(path: String): ProjectDependency = project(path, "dev")

val LomExtension.isFabric: Boolean get() = loaderKind.get() == LoaderKind.FABRIC
val LomExtension.isForge: Boolean get() = loaderKind.get() == LoaderKind.FORGE
val LomExtension.isNeoForge: Boolean get() = loaderKind.get() == LoaderKind.NEOFORGE
val LomExtension.isSplitSources: Boolean get() = isFabric // other loaders don't support split sources

val LomExtension.javaVersion: Provider<Int> get() = minecraftVersion.map {
    if (SemanticVersion.parse(it) > SemanticVersion(1, 20, 4, VersionType.RELEASE)) 21
    else 17
}