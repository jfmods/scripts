plugins {
    jf.`plugin-conventions`
}

dependencies {
    api(projects.convention)
//    api("net.fabricmc:fabric-loom:1.6-SNAPSHOT")
    api("dev.architectury.loom:dev.architectury.loom.gradle.plugin:1.9-SNAPSHOT") // https://maven.shedaniel.me/dev/architectury/loom/dev.architectury.loom.gradle.plugin/
    api("com.gradleup.shadow:shadow-gradle-plugin:8.3.5") // https://github.com/GradleUp/shadow/releases
}