plugins {
    jf.`plugin-conventions`
}

dependencies {
    api(projects.convention)
//    api("net.fabricmc:fabric-loom:1.6-SNAPSHOT")
    api("dev.architectury.loom:dev.architectury.loom.gradle.plugin:1.9-SNAPSHOT")
    api("com.gradleup.shadow:shadow-gradle-plugin:8.3.5")
}