package io.gitlab.jfronny.scripts

import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Nested
import org.jetbrains.annotations.ApiStatus.Internal

interface JfModExtension : LomExtension {
    val libJfVersion: Property<String>
    val configCompilerPluginVersion: Property<String>
    val fabricApiVersion: Property<String>
    val devOnly: Property<Boolean>
    @get:Nested val curseforge: JfCurseForgeExtension
    @get:Nested val modrinth: JfModrinthExtension

    @Internal
    override fun check(proj: Project) {
        super.check(proj)

        devOnly.finalizeValue()
        require(proj != proj.rootProject || !devOnly.getOrElse(false)) { "Root project may not be devOnly" }

        libJfVersion.finalizeValue()
        configCompilerPluginVersion.finalizeValue()
        fabricApiVersion.finalizeValue()
        curseforge.check(proj)
        modrinth.check(proj)
    }

    fun copyFrom(ext: JfModExtension) {
        super.copyFrom(ext)
        libJfVersion.convention(ext.libJfVersion)
        configCompilerPluginVersion.convention(ext.configCompilerPluginVersion)
        fabricApiVersion.convention(ext.fabricApiVersion)
    }

    fun isMod(proj: Project) = proj.extensions.findByName("jfMod") != null
    fun isFabric(proj: Project) = if (proj == this) isFabric else (proj.extensions.findByName("lom") as? LomExtension)?.isFabric ?: false
    fun isForge(proj: Project) = if (proj == this) isForge else (proj.extensions.findByName("lom") as? LomExtension)?.isNeoForge ?: false
    fun isNeoForge(proj: Project) = if (proj == this) isNeoForge else (proj.extensions.findByName("lom") as? LomExtension)?.isNeoForge ?: false
    fun isSplitSources(proj: Project) = if (proj == this) isSplitSources else (proj.extensions.findByName("lom") as? LomExtension)?.isSplitSources ?: false
    fun curseforge(closure: Action<JfCurseForgeExtension>) = closure.execute(curseforge)
    fun modrinth(closure: Action<JfModrinthExtension>) = closure.execute(modrinth)
}
