package io.gitlab.jfronny.scripts

import org.gradle.api.Project
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.Property
import org.jetbrains.annotations.ApiStatus.Internal

interface JfModrinthExtension {
    val projectId: Property<String>
    val requiredDependencies: ListProperty<String>
    val optionalDependencies: ListProperty<String>

    @Internal
    fun check(proj: Project) {
        projectId.finalizeValue()
        requiredDependencies.finalizeValue()
        optionalDependencies.finalizeValue()
        if (!projectId.isPresent) {
            require(requiredDependencies.map { it.isEmpty() }.getOrElse(true)) { "modrinth is not configured for a project id but has dependencies" }
            require(optionalDependencies.map { it.isEmpty() }.getOrElse(true)) { "modrinth is not configured for a project id but has dependencies" }
        }
    }
}