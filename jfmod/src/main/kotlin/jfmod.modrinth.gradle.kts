import io.gitlab.jfronny.scripts.*

plugins {
    id("jfmod")
    id("com.modrinth.minotaur")
}

val args = extensions.getByName("jfMod") as JfModExtension

val readmeFile = file("README.md")

modrinth {
    token.set(System.getenv()["MODRINTH_API_TOKEN"])
    projectId.set(args.modrinth.projectId)
    versionName.set("[${lom.minecraftVersion.get()}] $versionS")
    versionType.set(project.versionType.modrinthName)
    changelog.set(project.changelog)
    uploadFile.set(tasks.remapJar as Any)
    gameVersions.add(lom.minecraftVersion.get())
    loaders.add(args.loaderKind.get().modrinthName)
    args.modrinth.requiredDependencies.getOrElse(listOf()).forEach { required.project(it) }
    args.modrinth.optionalDependencies.getOrElse(listOf()).forEach { optional.project(it) }
    if (readmeFile.exists()) {
        syncBodyFrom.set(
            """${readmeFile.readText()}
<center>
  <a href="https://jfronny.gitlab.io/contact.html">
    <img alt="Suggestions and Support" src="https://img.shields.io/badge/Suggestions%20and%20Support-red?style=for-the-badge&logo=matrix">
  </a>
</center>"""
        )
    }
}

tasks.deployRelease.dependsOn(tasks.modrinth)
if (readmeFile.exists()) {
    tasks.deployRelease.dependsOn(tasks.modrinthSyncBody)
}