import io.gitlab.jfronny.scripts.*
import org.gradle.api.internal.project.ProjectStateInternal
import org.gradle.api.tasks.compile.JavaCompile
import org.gradle.kotlin.dsl.dependencies
import org.gradle.kotlin.dsl.withType

plugins {
    id("jf.autoversion")
    id("jf.maven-publish")
    id("lom")
}

val args = extensions.create<JfModExtension>("jfMod")
val lomArgs get() = extensions.getByName<LomExtension>("lom")
// Mirrored from lom.gradle.kts
args.loaderKind = LoaderKind.fromString(prop("loom.platform", "fabric"))
args.loaderKind.finalizeValue()

val isRoot = project == rootProject
if (!isRoot) (rootProject.extensions.findByName("jfMod") as JfModExtension?)?.let { args.copyFrom(it) }
lomArgs.copyFrom(args)

insertEarlyAfterEvaluate {
    args.check(project)

    if (!isRoot) versionS = rootProject.versionS

    lom {
        copyFrom(args)
    }

    fun DependencyHandlerScope.registerBom(notation: Any) {
        listOf<(Any) -> Dependency?>(
            this::modImplementation,
            this::modLocalRuntime,
            this::modApi,
            this::include,
            this::modCompileOnly
        ).forEach { function ->
            function(platform(notation))
        }
    }

    fun String.trimSnapshot() = if (endsWith("-SNAPSHOT")) dropLast(9) else this

    if (args.libJfVersion.isPresent) {
        dependencies {
            if (SemanticVersion.parse(args.libJfVersion.get().trimSnapshot()) > SemanticVersion.parse("3.14.2")) {
                if (args.isNeoForge(project)) {
                    registerBom("io.gitlab.jfronny.libjf.forge:libjf-bom:${args.libJfVersion.get()}")
                } else {
                    registerBom("io.gitlab.jfronny.libjf:libjf-bom:${args.libJfVersion.get()}")
                }
            }
            val dep = testAnnotationProcessor(annotationProcessor(
                "io.gitlab.jfronny.libjf:libjf-config-compiler-plugin-v2:${args.configCompilerPluginVersion.orElse(args.libJfVersion).get()}"
            )!!)!!
            if (lomArgs.isSplitSources) {
                clientAnnotationProcessor(dep)
            }
        }

        tasks.withType<JavaCompile> {
            options.compilerArgs.add("-AmodId=" + base.archivesName.get())
        }
    }
    if (args.fabricApiVersion.isPresent) {
        dependencies {
            registerBom("net.fabricmc.fabric-api:fabric-api-bom:${args.fabricApiVersion.get()}")
        }
    }

    if (args.curseforge.projectId.isPresent) apply(plugin = "jfmod.curseforge")
    if (args.modrinth.projectId.isPresent) apply(plugin = "jfmod.modrinth")
}

fun Project.pAfterEvaluate(action: (Project) -> Unit) =
    if ((this.state as ProjectStateInternal).hasCompleted()) action(this)
    else afterEvaluate { action(this) }

afterEvaluate {
    rootProject.allprojects.forEach {
        it.pAfterEvaluate { sub ->
            if (sub != this && args.isMod(sub)) {
                loom {
                    mods {
                        register(sub.name) {
                            sourceSet(sub.sourceSets.main.get())
                            if (args.isSplitSources(sub)) sourceSet(sub.sourceSets.client.get())
                        }
                        if (!sub.sourceSets.testmod.get().resources.isEmpty) {
                            register("${sub.name}-testmod") {
                                sourceSet(sub.sourceSets.testmod.get())
                            }
                        }
                    }
                }
            }
        }
    }

    val devOnly = args.devOnly.getOrElse(false)
    val hasTestmod = !sourceSets.testmod.get().resources.isEmpty
    if (!isRoot && !devOnly && hasTestmod) {
        //TODO register testmods of subprojects as testmodInclude automatically
//            val configuration = configurations.create("testmodJar").name
//            val testmodJarTask = tasks.named<Jar>("testmodJar").get()
//            artifacts.add(configuration, testmodJarTask.archiveFile.get().asFile) {
//                type = "jar"
//                builtBy(testmodJarTask)
//            }
//
//            self.dependencies {
//                testmodInclude(project(mapOf("path" to path, "configuration" to configuration)))
//            }
    }

    if (!isRoot) rootProject.dependencies {
        api(project(path = project.path, configuration = "shadow"))
        if (lomArgs.isSplitSources) clientImplementation(sourceSets.client.get().output)
        if (hasTestmod) testmodImplementation(project(mapOf("path" to project.path, "configuration" to "testmod")))

        if (!devOnly) include(project)
    }
}

if (!isRoot) {
    rootProject.tasks.deployDebug.dependsOn(tasks.deployDebug)
    rootProject.tasks.deployRelease.dependsOn(tasks.deployRelease)

    tasks.named("javadoc") { enabled = false }
}

val moveArtifacts by tasks.registering(Copy::class) {
    val suffix = if (flavour.isEmpty()) "" else "-$flavour"

    val sources = mapOf("latest$suffix.jar" to tasks.remapJar, "latest$suffix-dev.jar" to tasks.shadowJar)

    for (source in sources) {
        from(source.value.map { it.archiveFile }) {
            this.rename { source.key }
        }
        dependsOn(source.value)
    }

    into(layout.buildDirectory.dir("artifacts"))
}

tasks.deployDebug.dependsOn(moveArtifacts)
