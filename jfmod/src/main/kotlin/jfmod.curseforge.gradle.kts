import io.gitlab.jfronny.scripts.*
import net.darkhax.curseforgegradle.TaskPublishCurseForge

plugins {
    id("jfmod")
    id("net.darkhax.curseforgegradle")
}

val args = extensions.getByName("jfMod") as JfModExtension

val curseforge by tasks.registering(TaskPublishCurseForge::class) {
    if (System.getenv().containsKey("CURSEFORGE_API_TOKEN")) {
        val tk = System.getenv()["CURSEFORGE_API_TOKEN"]
        if ("debug" == tk) debugMode = true
        else apiToken = tk
    } else println("No CURSEFORGE_API_TOKEN specified")

    disableVersionDetection()
    upload(args.curseforge.projectId.get(), tasks.remapJar.get()) {
        releaseType = versionType.curseforgeName
        addModLoader(args.loaderKind.get().curseforgeName)
        addGameVersion(lom.minecraftVersion.get())
        addJavaVersion("Java " + lom.javaVersion.get())
        changelog = project.changelog
        changelogType = "markdown"
        displayName = "[${lom.minecraftVersion.get()}] $versionS"
        addRequirement(*args.curseforge.requiredDependencies.getOrElse(listOf()).toTypedArray())
        addOptional(*args.curseforge.optionalDependencies.getOrElse(listOf()).toTypedArray())
    }
}

tasks.deployRelease.dependsOn(curseforge)
