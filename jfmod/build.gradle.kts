plugins {
    jf.`plugin-conventions`
}

dependencies {
    api(projects.lom)
    implementation("com.modrinth.minotaur:Minotaur:2.8.7") // https://github.com/modrinth/minotaur/releases
    implementation("net.darkhax.curseforgegradle:CurseForgeGradle:1.1.26") // https://github.com/Darkhax/CurseForgeGradle
}
