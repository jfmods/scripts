plugins {
    jf.`plugin-conventions`
}

dependencies {
    api(projects.lom)
    implementation("com.modrinth.minotaur:Minotaur:2.8.7")
    implementation("net.darkhax.curseforgegradle:CurseForgeGradle:1.1.25")
}
