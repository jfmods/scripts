FROM archlinux:base-devel
LABEL authors="johannes"
LABEL org.opencontainers.image.title="CI with Wine"
LABEL org.opencontainers.image.description="Arch-based image containing a JDK, gradle, wine, WiX and supporting scripts"
LABEL org.opencontainers.image.authors="JFronny"
LABEL org.opencontainers.image.url="https://git.frohnmeyer-wds.de/Johannes/Scripts/src/branch/master/docker"
LABEL org.opencontainers.image.documentation="https://git.frohnmeyer-wds.de/Johannes/Scripts/src/branch/master/docker"
LABEL org.opencontainers.image.source="https://git.frohnmeyer-wds.de/Johannes/Scripts"
LABEL org.opencontainers.image.licenses="GPL-3.0-or-later"

RUN printf "[multilib]\nInclude = /etc/pacman.d/mirrorlist\n" >> /etc/pacman.conf
RUN pacman --noconfirm -Syu curl p7zip zip unzip jq git mdbook wine winetricks gnutls xorg-server-xvfb lib32-gnutls libunwind dpkg fakeroot jdk-openjdk &&\
    pacman --noconfirm -Sdd gradle &&\
    rm -rf /var/cache/pacman/pkg
RUN curl https://github.com/wixtoolset/wix3/releases/download/wix3112rtm/wix311-binaries.zip -Lo wix.zip &&\
    mkdir -p root/.wine/drive_c/Program\ Files\ \(x86\)/WiX\ Toolset\ v3.11/bin &&\
    unzip wix.zip -d root/.wine/drive_c/Program\ Files\ \(x86\)/WiX\ Toolset\ v3.11/bin &&\
    rm wix.zip
RUN curl https://api.adoptium.net/v3/binary/latest/22/ga/windows/x64/jdk/hotspot/normal/eclipse?project=jdk -Lo adoptium.zip &&\
    mkdir root/java &&\
    unzip adoptium.zip -d root/java &&\
    mv root/java/*/* root/java &&\
    rm adoptium.zip
ENV WINEDEBUG=-all
RUN wine wineboot --init &&\
    while pgrep wineserver > /dev/null; do sleep 1; done &&\
    winetricks --unattended win10 &&\
    winetricks --unattended --force dotnet48
RUN mv root/.wine/drive_c/Program\ Files\ \(x86\)/WiX\ Toolset\ v3.11/bin/light.exe root/.wine/drive_c/Program\ Files\ \(x86\)/WiX\ Toolset\ v3.11/bin/light.exe.original.exe &&\
    curl https://gitlab.com/api/v4/projects/34738832/jobs/artifacts/master/raw/dargl.exe?job=natives -Lo root/.wine/drive_c/Program\ Files\ \(x86\)/WiX\ Toolset\ v3.11/bin/light.exe &&\
    echo "-sval" > root/.wine/drive_c/Program\ Files\ \(x86\)/WiX\ Toolset\ v3.11/bin/light.exe.args.txt &&\
    mkdir -p /root/jpackage-win/bin &&\
    ln -s /root/java/jmods /root/jpackage-win/jmods &&\
    printf "#\!/bin/bash\n/usr/lib/jvm/default/bin/java \"\$@\"\n" > /root/jpackage-win/bin/java &&\
    chmod a+x /root/jpackage-win/bin/java &&\
    printf "#\!/bin/bash\n/usr/lib/jvm/default/bin/javac \"\$@\"\n" > /root/jpackage-win/bin/javac &&\
    chmod a+x /root/jpackage-win/bin/javac &&\
    printf "#\!/bin/bash\n/usr/lib/jvm/default/bin/jlink \"\$@\"\n" > /root/jpackage-win/bin/jlink &&\
    chmod a+x /root/jpackage-win/bin/jlink &&\
    printf "#\!/bin/bash\nwine64 /root/java/bin/jpackage.exe \"\$@\"\n" > /root/jpackage-win/bin/jpackage &&\
    chmod a+x /root/jpackage-win/bin/jpackage
